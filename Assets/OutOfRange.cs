﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OutOfRange : MonoBehaviour {


	public bool isLeftSide;
	public bool isRightSide;


	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other) {
		if (isLeftSide) {
			if (Abilities.instance.rightAbility1 == true) {
				Score.ScorePoint (2, 2);
				Abilities.instance.rightAbility1 = false;
				//AbilityUpdateUI.instance.UpdateUI ();
			} else {
				Score.ScorePoint (2);
				//AbilityUpdateUI.instance.UpdateUI ();
			}
			if (Abilities.instance.rightAbility2 == true) {
				Ball.instance.speed += 5f;
				Abilities.instance.rightAbility2 = false;
				//AbilityUpdateUI.instance.UpdateUI ();
			} else {
				Ball.instance.speed = 5f;
			}
			if (Abilities.instance.rightAbility3 == true) {
				Bumper.bump2Speed += 5f;
				Abilities.instance.rightAbility3 = false;
				//AbilityUpdateUI.instance.UpdateUI ();
			} else {
				Bumper.bump2Speed = 5f;
			}

		} else if(isRightSide){
			if (Abilities.instance.leftAbility1 == true) {
				Score.ScorePoint (1, 2);
				Abilities.instance.leftAbility1 = false;
				//AbilityUpdateUI.instance.UpdateUI ();

			} else {
				Score.ScorePoint (1, 2);
				AbilityUpdateUI.instance.UpdateUI ();
			}
			if (Abilities.instance.leftAbility2 == true) {
				Ball.instance.speed += 5f;
				Abilities.instance.leftAbility2 = false;
				//AbilityUpdateUI.instance.UpdateUI ();

			} else {
				Ball.instance.speed = 5f;
				//AbilityUpdateUI.instance.UpdateUI ();
			}
			if (Abilities.instance.leftAbility3 == true) {
				Bumper.bump1Speed += 5f;
				Abilities.instance.leftAbility3 = false;
				//AbilityUpdateUI.instance.UpdateUI ();

			} else {
				Bumper.bump1Speed = 5f;
				//AbilityUpdateUI.instance.UpdateUI ();
			}
		}

		other.GetComponent<Ball> ().Movement ();

	}
}
