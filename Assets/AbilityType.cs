﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityType : MonoBehaviour {

	public int ability;


	void Start(){
		if (ability == 1) {
			GetComponent<Renderer> ().material.color = Color.red;
		} else if (ability == 2) {
			GetComponent<Renderer> ().material.color = Color.blue;
		} else if (ability == 3) {
			GetComponent<Renderer> ().material.color = Color.green;
		}
	}
	void OnTriggerEnter(Collider other) {
		Abilities.instance.ApplyAbility (Ball.lastSideTouched, ability);
	}
}
