﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateScoreUI : MonoBehaviour {


	public static UpdateScoreUI instance = null; 
	public Text scoreLeftText;
	public Text scoreRightText;

	void Awake() {
		if(instance == null) {
			instance = this;
		} else if(instance != this) {
			Destroy(this.gameObject);
		}
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void UpdateUI() {
		scoreLeftText.text = "Score: " + Score.scoreLeft;
		scoreRightText.text = "Score: " + Score.scoreRight;
	}
}
