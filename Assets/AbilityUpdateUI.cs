﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityUpdateUI : MonoBehaviour {

	public static AbilityUpdateUI instance = null; 
	public Text abilityLeftText;
	public Text abilityRightText;

	void Awake() {
		if(instance == null) {
			instance = this;
		} else if(instance != this) {
			Destroy(this.gameObject);
		}
	}
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	public void UpdateUI() {
		Abilities.instance.AbilityList ();
		abilityLeftText.text = "Ability: " + Abilities.instance.leftAbilityList;
		abilityRightText.text = "Ability: " + Abilities.instance.rightAbilityList;;
	}
}
