﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

	public static int scoreLeft = -1;
	public static int scoreRight = -1;


	// Use this for initialization
	void Start () {
		scoreLeft = 0;
		scoreRight = 0;

	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public static void ScorePoint(int side, int mod = 1) {

		if (side == 1) {
			scoreLeft += (1 * mod);
			Debug.Log ("score left side");
		}

		if (side == 2) {
			scoreRight += (1 * mod);
			Debug.Log ("score right side");
		}

		UpdateScoreUI.instance.UpdateUI ();
	}
}
